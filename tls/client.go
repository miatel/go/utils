package tls

import (
	"crypto/tls"
	"crypto/x509"
	"os"
)

type ClientConfig struct {
	CertFile   string      `env:"CERT_FILE" yaml:"cert_file"`
	KeyFile    string      `env:"KEY_FILE" yaml:"key_file"`
	CAFiles    []string    `env:"CA_FILES" yaml:"ca_files"`
	ServerName string      `env:"SERVER_NAME" yaml:"server_name"`
	MinVersion VersionType `env:"MIN_VERSION" yaml:"min_version"`
}

func (c *ClientConfig) TLSConfig() (config *tls.Config, err error) {
	config = &tls.Config{
		ServerName: c.ServerName,
		MinVersion: uint16(c.MinVersion),
	}

	if c.CertFile != "" && c.KeyFile != "" {
		config.Certificates = make([]tls.Certificate, 1)
		config.Certificates[0], err = tls.LoadX509KeyPair(c.CertFile, c.KeyFile)
		if err != nil {
			return nil, err
		}
	}

	if len(c.CAFiles) > 0 {
		certPool := x509.NewCertPool()
		for _, certFileName := range c.CAFiles {
			var certFile []byte
			if certFile, err = os.ReadFile(certFileName); err != nil {
				return nil, err
			}
			certPool.AppendCertsFromPEM(certFile)
		}
		config.RootCAs = certPool
	}

	return config, nil
}
