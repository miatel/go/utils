package tls

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"os"
)

type ServerConfig struct {
	CertFile       string         `env:"CERT_FILE" yaml:"cert_file"`
	KeyFile        string         `env:"KEY_FILE" yaml:"key_file"`
	ClientAuthType ClientAuthType `env:"CLIENT_AUTH_TYPE" yaml:"client_auth_type"`
	ClientCAs      []string       `env:"CLIENT_CA_FILES" yaml:"client_ca_files"`
	MinVersion     VersionType    `env:"MIN_VERSION" yaml:"min_version"`
}

func (c *ServerConfig) TLSConfig() (config *tls.Config, err error) {
	if c.CertFile == "" || c.KeyFile == "" {
		return nil, errors.New("'cert_file' and 'key_file' must be defined")
	}

	config = &tls.Config{
		ClientAuth: tls.ClientAuthType(c.ClientAuthType),
		MinVersion: uint16(c.MinVersion),
	}

	config.Certificates = make([]tls.Certificate, 1)
	config.Certificates[0], err = tls.LoadX509KeyPair(c.CertFile, c.KeyFile)
	if err != nil {
		return nil, err
	}

	if len(c.ClientCAs) > 0 {
		certPool := x509.NewCertPool()
		for _, certFileName := range c.ClientCAs {
			var certFile []byte
			if certFile, err = os.ReadFile(certFileName); err != nil {
				return nil, err
			} else {
				certPool.AppendCertsFromPEM(certFile)
			}
		}
		config.ClientCAs = certPool
	}

	if len(c.ClientCAs) > 0 && config.ClientAuth == tls.NoClientCert {
		return nil, errors.New("'client_ca_files' have been configured without a 'client_auth_type'")
	}

	return config, nil
}
