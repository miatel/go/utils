package database

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"

	"github.com/jackc/pgx/v5/pgtype"
)

type JSONBCodec struct{}

func (JSONBCodec) FormatSupported(format int16) bool {
	return format == pgtype.TextFormatCode || format == pgtype.BinaryFormatCode
}

func (JSONBCodec) PreferredFormat() int16 {
	return pgtype.TextFormatCode
}

func (JSONBCodec) PlanEncode(m *pgtype.Map, oid uint32, format int16, value any) pgtype.EncodePlan {
	switch format {
	case pgtype.BinaryFormatCode:
		plan := JSONCodec{}.PlanEncode(m, oid, pgtype.TextFormatCode, value)
		if plan != nil {
			return &encodePlanJSONBCodecBinary{textPlan: plan}
		}

	case pgtype.TextFormatCode:
		return JSONCodec{}.PlanEncode(m, oid, format, value)
	}

	return nil
}

type encodePlanJSONBCodecBinary struct {
	textPlan pgtype.EncodePlan
}

func (plan *encodePlanJSONBCodecBinary) Encode(value any, buf []byte) ([]byte, error) {
	buf = append(buf, 1)
	return plan.textPlan.Encode(value, buf)
}

func (JSONBCodec) PlanScan(m *pgtype.Map, oid uint32, format int16, target any) pgtype.ScanPlan {
	switch format {
	case pgtype.BinaryFormatCode:
		plan := JSONCodec{}.PlanScan(m, oid, pgtype.TextFormatCode, target)
		if plan != nil {
			return &scanPlanJSONBCodecBinary{textPlan: plan}
		}

	case pgtype.TextFormatCode:
		return JSONCodec{}.PlanScan(m, oid, format, target)
	}

	return nil
}

type scanPlanJSONBCodecBinary struct {
	textPlan pgtype.ScanPlan
}

func (plan *scanPlanJSONBCodecBinary) Scan(src []byte, dst any) error {
	if src == nil {
		return plan.textPlan.Scan(src, dst)
	}

	if len(src) == 0 {
		return fmt.Errorf("jsonb too short")
	}

	if src[0] != 1 {
		return fmt.Errorf("unknown jsonb version number %d", src[0])
	}

	return plan.textPlan.Scan(src[1:], dst)
}

func (JSONBCodec) DecodeDatabaseSQLValue(m *pgtype.Map, oid uint32, format int16, src []byte) (driver.Value, error) {
	return (&pgtype.JSONBCodec{Marshal: json.Marshal, Unmarshal: json.Unmarshal}).DecodeDatabaseSQLValue(m, oid, format, src)
}

func (JSONBCodec) DecodeValue(_ *pgtype.Map, _ uint32, format int16, src []byte) (any, error) {
	if src == nil {
		return nil, nil
	}

	switch format {
	case pgtype.BinaryFormatCode:
		if len(src) == 0 {
			return nil, fmt.Errorf("jsonb too short")
		}

		if src[0] != 1 {
			return nil, fmt.Errorf("unknown jsonb version number %d", src[0])
		}

		src = src[1:]

	case pgtype.TextFormatCode:

	default:
		return nil, fmt.Errorf("unknown format code: %v", format)
	}

	var dst JSON
	if err := dst.UnmarshalJSON(src); err != nil {
		return nil, err
	}

	return dst, nil
}
