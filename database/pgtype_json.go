package database

import (
	"database/sql/driver"
	"encoding/json"

	"github.com/jackc/pgx/v5/pgtype"
)

type JSONCodec struct{}

func (JSONCodec) FormatSupported(format int16) bool {
	return format == pgtype.TextFormatCode || format == pgtype.BinaryFormatCode
}

func (JSONCodec) PreferredFormat() int16 {
	return pgtype.TextFormatCode
}

func (c JSONCodec) PlanEncode(m *pgtype.Map, oid uint32, format int16, value any) pgtype.EncodePlan {
	switch value.(type) {
	case JSON:
		return encodePlanJSONCodec{}
	}

	// Because anything can be marshalled the normal wrapping in Map.PlanScan doesn't get a chance to run. So try the
	// appropriate wrappers here.
	for _, f := range []pgtype.TryWrapEncodePlanFunc{
		pgtype.TryWrapDerefPointerEncodePlan,
		pgtype.TryWrapFindUnderlyingTypeEncodePlan,
	} {
		if wrapperPlan, nextValue, ok := f(value); ok {
			if nextPlan := c.PlanEncode(m, oid, format, nextValue); nextPlan != nil {
				wrapperPlan.SetNext(nextPlan)
				return wrapperPlan
			}
		}
	}

	return (&pgtype.JSONCodec{Marshal: json.Marshal, Unmarshal: json.Unmarshal}).PlanEncode(m, oid, format, value)
}

type encodePlanJSONCodec struct{}

func (encodePlanJSONCodec) Encode(value any, buf []byte) ([]byte, error) {
	jsonBytes, err := value.(JSON).MarshalJSON()
	if err != nil {
		return nil, err
	}

	buf = append(buf, jsonBytes...)

	return buf, nil
}

func (JSONCodec) PlanScan(m *pgtype.Map, oid uint32, format int16, target any) pgtype.ScanPlan {
	switch target.(type) {
	case *JSON:
		return scanPlanJSONCodec{}
	}

	return (&pgtype.JSONCodec{Marshal: json.Marshal, Unmarshal: json.Unmarshal}).PlanScan(m, oid, format, target)
}

type scanPlanJSONCodec struct{}

func (scanPlanJSONCodec) Scan(src []byte, dst any) error {
	value := dst.(*JSON)

	if src == nil {
		*value = nil
	}

	return value.UnmarshalJSON(src)
}

func (c JSONCodec) DecodeDatabaseSQLValue(m *pgtype.Map, oid uint32, format int16, src []byte) (driver.Value, error) {
	return (&pgtype.JSONCodec{Marshal: json.Marshal, Unmarshal: json.Unmarshal}).DecodeDatabaseSQLValue(m, oid, format, src)
}

func (c JSONCodec) DecodeValue(m *pgtype.Map, oid uint32, format int16, src []byte) (any, error) {
	if src == nil {
		return nil, nil
	}

	var dst JSON
	if err := dst.UnmarshalJSON(src); err != nil {
		return nil, err
	}

	return dst, nil
}
