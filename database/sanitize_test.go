package database_test

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/miatel/go/utils/database"
)

func SanitizeOld(ident string) string {
	s := strings.ReplaceAll(ident, string([]byte{0}), "")
	return `"` + strings.ReplaceAll(s, `"`, `""`) + `"`
}

func BenchmarkSanitizeOld(b *testing.B) {
	tests := []struct {
		Name  string
		Ident string
	}{
		{
			Name:  "Simple",
			Ident: `column1`,
		},
		{
			Name:  "Contain 0 byte",
			Ident: `col` + string([]byte{0}) + `umn1`,
		},
		{
			Name:  "Contain \" symbol",
			Ident: `"column1"`,
		},
	}

	for _, test := range tests {
		b.Run(test.Name, func(b *testing.B) {
			b.ReportAllocs()
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				_ = SanitizeOld(test.Ident)
			}
		})
	}
}

func BenchmarkSanitize(b *testing.B) {
	tests := []struct {
		Name  string
		Ident string
	}{
		{
			Name:  "Simple",
			Ident: `column1`,
		},
		{
			Name:  "Contain 0 byte",
			Ident: `col` + string([]byte{0}) + `umn1`,
		},
		{
			Name:  "Contain \" symbol",
			Ident: `"column1"`,
		},
	}

	for _, test := range tests {
		b.Run(test.Name, func(b *testing.B) {
			b.ReportAllocs()
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				_ = database.Sanitize(test.Ident)
			}
		})
	}
}

func TestSanitize(t *testing.T) {
	tests := []struct {
		Name   string
		Ident  string
		Result string
	}{
		{
			Name:   "Simple",
			Ident:  `column1`,
			Result: `"column1"`,
		},
		{
			Name:   "Contain 0 byte",
			Ident:  `col` + string([]byte{0}) + `umn1`,
			Result: `"column1"`,
		},
		{
			Name:   "Contain \" symbol",
			Ident:  `"column1"`,
			Result: `"""column1"""`,
		},
	}

	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			result := database.Sanitize(test.Ident)
			require.Equal(t, test.Result, result)
		})
	}
}
