package cdc

import "errors"

var (
	ErrKeyNotDefined  = errors.New("key not defined")
	ErrUnexpectedNULL = errors.New("unexpected NULL")
)
