package cdc

import (
	"reflect"

	"github.com/fxamacker/cbor/v2"
	"github.com/jackc/pgx/v5/pgtype"

	utilsDatabase "gitlab.com/miatel/go/utils/database"
)

var (
	encMode cbor.EncMode
	decMode cbor.DecMode
)

func init() {
	var err error

	tags := cbor.NewTagSet()
	tags.Add(
		cbor.TagOptions{EncTag: cbor.EncTagRequired, DecTag: cbor.DecTagRequired},
		reflect.TypeOf(utilsDatabase.JSON{}),
		999,
	)
	tags.Add(
		cbor.TagOptions{EncTag: cbor.EncTagRequired, DecTag: cbor.DecTagRequired},
		reflect.TypeOf(pgtype.Numeric{}),
		998,
	)

	encOpts := cbor.EncOptions{
		Time:        cbor.TimeUnixMicro,
		TimeTag:     cbor.EncTagRequired,
		IndefLength: cbor.IndefLengthForbidden,
	}
	encMode, err = encOpts.EncModeWithTags(tags)
	if err != nil {
		panic(err)
	}

	decOpts := cbor.DecOptions{
		TimeTag:          cbor.DecTagRequired,
		MaxNestedLevels:  128,
		MaxArrayElements: 20480, // размер сообщения в Kafka 2048000 байт / 100 символов в строке файла
		MaxMapPairs:      2048,
		IndefLength:      cbor.IndefLengthForbidden,
		IntDec:           cbor.IntDecConvertSigned,
	}
	decMode, err = decOpts.DecModeWithTags(tags)
	if err != nil {
		panic(err)
	}
}
