package cdc

type Key []any
type cborKey Key

func (v *Key) MarshalCBOR() ([]byte, error) {
	return encMode.Marshal((*cborKey)(v))
}

func (v *Key) UnmarshalCBOR(raw []byte) error {
	return decMode.Unmarshal(raw, (*cborKey)(v))
}
