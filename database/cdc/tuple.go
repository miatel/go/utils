package cdc

import (
	"fmt"

	utilsDatabase "gitlab.com/miatel/go/utils/database"
)

type Columns []string
type Tuple map[string]any

func NewTuple(columns Columns, row []any) (*Tuple, error) {
	if len(columns) != len(row) {
		return nil, fmt.Errorf("wrong number of columns %d != %d", len(columns), len(row))
	}

	tuple := make(Tuple, len(columns))
	for idx, column := range columns {
		tuple[column] = row[idx]
	}

	return &tuple, nil
}

func (v *Columns) String() string {
	if v == nil {
		return "nil"
	}

	b := make([]byte, 0, 2048)
	if len(*v) > 0 {
		b = append(b, '[')
		for _, value := range *v {
			b = fmt.Appendf(b, "%s,", utilsDatabase.Sanitize(value))
		}
		b = b[:len(b)-1]
		b = append(b, ']')
	} else {
		b = append(b, "[]"...)
	}

	return string(b)
}

func (v *Tuple) String() string {
	if v == nil {
		return "nil"
	}

	b := make([]byte, 0, 2048)
	if len(*v) > 0 {
		b = append(b, '{')
		for key, value := range *v {
			b = append(b, key...)
			b = append(b, ':')
			b = fmt.Appendf(b, "%v ", value)
		}
		b = b[:len(b)-1]
		b = append(b, '}')
	} else {
		b = append(b, "{}"...)
	}

	return string(b)
}

func TupleValue[T any](tuple Tuple, key string) (*T, error) {
	var (
		ok  bool
		val any
		ret T
	)

	if val, ok = tuple[key]; !ok {
		return nil, fmt.Errorf("tuple does not contain the %q key", key)
	}

	if val == nil {
		return nil, ErrUnexpectedNULL
	}

	if ret, ok = val.(T); !ok {
		return nil, fmt.Errorf("tuple contains key %q of the wrong type %T", key, val)
	}

	return &ret, nil
}
