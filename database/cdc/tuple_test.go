package cdc_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/miatel/go/utils/database/cdc"
)

var testTuple = cdc.Tuple{
	"key0":  byte('0'),
	"key1":  []byte("value1"),
	"key2":  "value2",
	"key3":  int8(-127),
	"key4":  int16(32767),
	"key5":  int32(2147483647),
	"key6":  int64(-9223372036854775808),
	"key7":  true,
	"key8":  float32(942.392),
	"key9":  float64(-31283.45345392),
	"key10": nil,
}

func TestTupleValue_EmptyKeyErr(t *testing.T) {
	value, err := cdc.TupleValue[int](testTuple, "")
	require.Error(t, err, "empty key error is expected")
	require.Nil(t, value)
}

func TestTupleValue_NonExistingKeyErr(t *testing.T) {
	value, err := cdc.TupleValue[int](testTuple, "non-existing-key")
	require.Error(t, err, "non-existing key error is expected")
	require.Nil(t, value)
}

func TestTupleValue_WrongTypeErr(t *testing.T) {
	value, err := cdc.TupleValue[bool](testTuple, "key0")
	require.Error(t, err, "wrong value type error is expected")
	require.Nil(t, value)
}

func TestTupleValue_Byte(t *testing.T) {
	value, err := cdc.TupleValue[byte](testTuple, "key0")
	require.NoError(t, err)
	require.NotNil(t, value)
	require.Equal(t, byte('0'), *value)
}

func TestTupleValue_ByteSlice(t *testing.T) {
	value, err := cdc.TupleValue[[]byte](testTuple, "key1")
	require.NoError(t, err)
	require.NotNil(t, value)
	require.Equal(t, []byte("value1"), *value)
}

func TestTupleValue_String(t *testing.T) {
	value, err := cdc.TupleValue[string](testTuple, "key2")
	require.NoError(t, err)
	require.NotNil(t, value)
	require.Equal(t, "value2", *value)
}

func TestTupleValue_Int8(t *testing.T) {
	value, err := cdc.TupleValue[int8](testTuple, "key3")
	require.NoError(t, err)
	require.NotNil(t, value)
	require.Equal(t, int8(-127), *value)
}

func TestTupleValue_Int16(t *testing.T) {
	value, err := cdc.TupleValue[int16](testTuple, "key4")
	require.NoError(t, err)
	require.NotNil(t, value)
	require.Equal(t, int16(32767), *value)
}

func TestTupleValue_Int32(t *testing.T) {
	value, err := cdc.TupleValue[int32](testTuple, "key5")
	require.NoError(t, err)
	require.NotNil(t, value)
	require.Equal(t, int32(2147483647), *value)
}

func TestTupleValue_Int64(t *testing.T) {
	value, err := cdc.TupleValue[int64](testTuple, "key6")
	require.NoError(t, err)
	require.NotNil(t, value)
	require.Equal(t, int64(-9223372036854775808), *value)
}

func TestTupleValue_Boolean(t *testing.T) {
	value, err := cdc.TupleValue[bool](testTuple, "key7")
	require.NoError(t, err)
	require.NotNil(t, value)
	require.Equal(t, true, *value)
}

func TestTupleValue_Float32(t *testing.T) {
	value, err := cdc.TupleValue[float32](testTuple, "key8")
	require.NoError(t, err)
	require.NotNil(t, value)
	require.Equal(t, float32(942.392), *value)
}

func TestTupleValue_Float64(t *testing.T) {
	value, err := cdc.TupleValue[float64](testTuple, "key9")
	require.NoError(t, err)
	require.NotNil(t, value)
	require.Equal(t, float64(-31283.45345392), *value)
}

func TestTupleValue_NilValue(t *testing.T) {
	value, err := cdc.TupleValue[int](testTuple, "key10")
	require.Error(t, err)
	require.Equal(t, cdc.ErrUnexpectedNULL, err)
	require.Nil(t, value)
}
