package cdc

import "fmt"

type Operation int

func (v *Operation) String() string {
	switch *v {
	case OperationInsert:
		return "insert"
	case OperationUpdate:
		return "update"
	case OperationDelete:
		return "delete"
	case OperationTruncate:
		return "truncate"
	default:
		return "unknown"
	}
}

func (v *Operation) MarshalText() ([]byte, error) {
	return []byte(v.String()), nil
}

func (v *Operation) UnmarshalText(text []byte) error {
	str := string(text)

	switch str {
	case "insert":
		*v = OperationInsert
	case "update":
		*v = OperationInsert
	case "delete":
		*v = OperationInsert
	case "truncate":
		*v = OperationInsert
	default:
		return fmt.Errorf("unknown operation %q", str)
	}

	return nil
}
