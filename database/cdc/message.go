package cdc

import (
	"errors"
	"fmt"
	"time"

	_ "github.com/mailru/easyjson/gen"
)

//go:generate easyjson $GOFILE

//easyjson:json
type Message struct {
	Operation Operation `cbor:"1,keyasint" json:"operation,string"`

	Schema string `cbor:"2,keyasint" json:"schema"`
	Table  string `cbor:"3,keyasint" json:"table"`

	Before *Tuple `cbor:"4,keyasint,omitempty" json:"before,omitempty"`
	After  *Tuple `cbor:"5,keyasint,omitempty" json:"after,omitempty"`

	PK *Columns `cbor:"6,keyasint,omitempty" json:"pk,omitempty"`

	CommitTime time.Time `cbor:"7,keyasint" json:"commit_time"`
}

type cborMessage Message

func (v *Message) MarshalCBOR() ([]byte, error) {
	return encMode.Marshal((*cborMessage)(v))
}

func (v *Message) UnmarshalCBOR(raw []byte) error {
	return decMode.Unmarshal(raw, (*cborMessage)(v))
}

func (v *Message) String() string {
	b := make([]byte, 0, 2048)
	b = append(b, "op="...)
	b = append(b, v.Operation.String()...)
	b = append(b, " schema="...)
	b = append(b, v.Schema...)
	b = append(b, " table="...)
	b = append(b, v.Table...)
	b = append(b, " before="...)
	b = append(b, v.Before.String()...)
	b = append(b, " after="...)
	b = append(b, v.After.String()...)
	b = append(b, " pk="...)
	b = append(b, v.After.String()...)
	b = append(b, " commit_time="...)
	b = fmt.Appendf(b, "%v", v.CommitTime)

	return string(b)
}

func (v *Message) Validate() error {
	switch v.Operation {
	case OperationInsert:
		if v.After == nil {
			return errors.New("after not defined")
		}

	case OperationUpdate:
		if v.Before == nil && (v.PK == nil || len(*v.PK) == 0) {
			return errors.New("before not defined")
		}
		if v.After == nil {
			return errors.New("after not defined")
		}

	case OperationDelete:
		if v.Before == nil {
			return errors.New("before not defined")
		}

	case OperationTruncate:

	default:
		return errors.New("unknown operation")
	}

	if v.Schema == "" {
		return errors.New("schema empty")
	}
	if v.Table == "" {
		return errors.New("table empty")
	}

	return nil
}

func (v *Message) Key(redefine Columns) (Key, error) {
	var columns Columns

	switch {
	case len(redefine) > 0:
		columns = redefine

	case v.PK != nil && len(*v.PK) > 0:
		columns = *v.PK

	default:
		return nil, ErrKeyNotDefined
	}

	var tuple Tuple
	if v.After != nil {
		tuple = *v.After
	} else if v.Before != nil {
		tuple = *v.Before
	} else {
		return nil, ErrKeyNotDefined
	}

	key := make(Key, 0, len(columns))
	for _, column := range columns {
		if val, ok := tuple[column]; ok {
			key = append(key, val)
		} else {
			return nil, fmt.Errorf("key column %q not found", column)
		}
	}

	return key, nil
}
