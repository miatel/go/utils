package cdc

const (
	OperationInsert   Operation = 0
	OperationUpdate   Operation = 1
	OperationDelete   Operation = 2
	OperationTruncate Operation = 3
)
