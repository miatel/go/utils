package database

import (
	"errors"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/miatel/go/log"
)

type Config struct {
	LogLevel       *log.Level `env:"LOG_LEVEL" yaml:"log_level"`
	DSN            string     `env:"DSN" yaml:"dsn"`
	ConnectTimeout *uint      `env:"CONNECT_TIMEOUT" yaml:"connect_timeout"`
}

func (v *Config) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.DSN == "" {
		return errors.New("'dsn' invalid value")
	}
	if v.ConnectTimeout != nil && *v.ConnectTimeout < 1000 {
		return errors.New("'connect_timeout' too small")
	}

	return nil
}

func (v *Config) PgConnConfig() (*pgconn.Config, error) {
	connConfig, err := pgconn.ParseConfig(v.DSN)
	if err != nil {
		return nil, err
	}

	if v.ConnectTimeout != nil {
		connConfig.ConnectTimeout = time.Duration(*v.ConnectTimeout) * time.Millisecond
	}

	return connConfig, nil
}

func (v *Config) ConnConfig(logger log.Logger) (*pgx.ConnConfig, error) {
	connConfig, err := pgx.ParseConfig(v.DSN)
	if err != nil {
		return nil, err
	}

	if v.ConnectTimeout != nil {
		connConfig.ConnectTimeout = time.Duration(*v.ConnectTimeout) * time.Millisecond
	}

	if logger != nil {
		if v.LogLevel != nil {
			logger.SetLevel(*v.LogLevel)
		}

		connConfig.Tracer = newLogger(logger).TraceLog()
	}

	return connConfig, nil
}

func (v *Config) PoolConfig(logger log.Logger) (*pgxpool.Config, error) {
	poolConfig, err := pgxpool.ParseConfig(v.DSN)
	if err != nil {
		return nil, err
	}

	if v.ConnectTimeout != nil {
		poolConfig.ConnConfig.ConnectTimeout = time.Duration(*v.ConnectTimeout) * time.Millisecond
	}

	if logger != nil {
		if v.LogLevel != nil {
			logger.SetLevel(*v.LogLevel)
		}

		poolConfig.ConnConfig.Tracer = newLogger(logger).TraceLog()
	}

	return poolConfig, nil
}
