package database

import (
	"database/sql/driver"
	"fmt"

	"github.com/jackc/pgx/v5/pgtype"
)

type QCharCodec struct{}

func (QCharCodec) FormatSupported(format int16) bool {
	return format == pgtype.TextFormatCode || format == pgtype.BinaryFormatCode
}

func (QCharCodec) PreferredFormat() int16 {
	return pgtype.BinaryFormatCode
}

func (QCharCodec) PlanEncode(m *pgtype.Map, oid uint32, format int16, value any) pgtype.EncodePlan {
	switch format {
	case pgtype.TextFormatCode, pgtype.BinaryFormatCode:
		switch value.(type) {
		case string:
			return encodePlanQcharCodecString{}
		}
	}

	return pgtype.QCharCodec{}.PlanEncode(m, oid, format, value)
}

type encodePlanQcharCodecString struct{}

func (encodePlanQcharCodecString) Encode(value any, buf []byte) ([]byte, error) {
	s := value.(string)
	if len(s) != 1 {
		return nil, fmt.Errorf(`%v cannot be encoded to "char"`, s)
	}

	buf = append(buf, s[0])

	return buf, nil
}

func (QCharCodec) PlanScan(m *pgtype.Map, oid uint32, format int16, target any) pgtype.ScanPlan {
	switch format {
	case pgtype.TextFormatCode, pgtype.BinaryFormatCode:
		switch target.(type) {
		case *string:
			return scanPlanQcharCodecString{}
		}
	}

	return pgtype.QCharCodec{}.PlanScan(m, oid, format, target)
}

type scanPlanQcharCodecString struct{}

func (scanPlanQcharCodecString) Scan(src []byte, dst any) error {
	if src == nil {
		return fmt.Errorf("cannot scan NULL into %T", dst)
	}

	if len(src) > 1 {
		return fmt.Errorf(`invalid length for "char": %v`, len(src))
	}

	s := dst.(*string)
	// In the text format the zero value is returned as a zero byte value instead of 0
	if len(src) == 0 {
		*s = string([]byte{0})
	} else {
		*s = string(src[0])
	}

	return nil
}

func (c QCharCodec) DecodeDatabaseSQLValue(m *pgtype.Map, oid uint32, format int16, src []byte) (driver.Value, error) {
	if src == nil {
		return nil, nil
	}

	var s string
	err := codecScan(c, m, oid, format, src, &s)
	if err != nil {
		return nil, err
	}

	return s, nil
}

func (c QCharCodec) DecodeValue(m *pgtype.Map, oid uint32, format int16, src []byte) (any, error) {
	if src == nil {
		return nil, nil
	}

	var s string
	err := codecScan(c, m, oid, format, src, &s)
	if err != nil {
		return nil, err
	}

	return s, nil
}
