package database

import (
	"context"

	pgx "github.com/jackc/pgx/v5/tracelog"
	"gitlab.com/miatel/go/log"
)

type Logger struct {
	log.Logger
}

func newLogger(logger log.Logger) *Logger {
	return &Logger{
		Logger: logger,
	}
}

func (v *Logger) Log(_ context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	logger := v.Logger

	if len(data) > 0 {
		logger = logger.WithFields(data)
	}

	switch level {
	case pgx.LogLevelNone:
	case pgx.LogLevelError:
		logger.Error(msg)
	case pgx.LogLevelWarn:
		logger.Warn(msg)
	case pgx.LogLevelInfo:
		logger.Info(msg)
	case pgx.LogLevelDebug:
		logger.Debug(msg)
	default:
		logger.Trace(msg)
	}
}

func (v *Logger) Level() (level pgx.LogLevel) {
	switch v.Logger.GetLevel() {
	case log.PanicLevel:
		fallthrough
	case log.FatalLevel:
		fallthrough
	case log.ErrorLevel:
		level = pgx.LogLevelError
	case log.WarnLevel:
		level = pgx.LogLevelWarn
	case log.NoticeLevel:
		level = pgx.LogLevelWarn
	case log.InfoLevel:
		level = pgx.LogLevelInfo
	case log.DebugLevel:
		level = pgx.LogLevelDebug
	case log.TraceLevel:
		level = pgx.LogLevelTrace
	default:
	}

	return level
}

func (v *Logger) TraceLog() *pgx.TraceLog {
	return &pgx.TraceLog{
		Logger:   v,
		LogLevel: v.Level(),
	}
}
