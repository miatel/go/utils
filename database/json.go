package database

import (
	_ "github.com/mailru/easyjson/gen"
)

//go:generate easyjson $GOFILE

//easyjson:json
type JSON map[string]any
