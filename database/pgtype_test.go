package database_test

import (
	"encoding/json"
	"testing"

	"github.com/jackc/pgx/v5/pgtype"
)

var jsonData = []byte(`{"glossary":{"GlossDiv":{"GlossList":{"GlossEntry":{"Abbrev":"ISO 8879:1986","Acronym":"SGML","GlossDef":{"GlossSeeAlso":["GML","XML"],"para":"A meta-markup language, used to create markup languages such as DocBook."},"GlossSee":"markup","GlossTerm":"Standard Generalized Markup Language","ID":"SGML","SortAs":"SGML"}},"title":"S"},"title":"example glossary"}}`)

func BenchmarkJson(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		_, _ = (&pgtype.JSONCodec{Marshal: json.Marshal, Unmarshal: json.Unmarshal}).DecodeValue(nil, pgtype.JSONOID, pgtype.TextFormatCode, jsonData)
	}
}

func BenchmarkEasyJson(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		_, _ = (&pgtype.JSONCodec{Marshal: json.Marshal, Unmarshal: json.Unmarshal}).DecodeValue(nil, pgtype.JSONOID, pgtype.TextFormatCode, jsonData)
	}
}
