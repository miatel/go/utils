package logger

import (
	"strings"

	"gitlab.com/miatel/go/log"
)

func New(c *Config) (logger log.Logger, err error) {
	switch strings.ToLower(c.Dest) {
	case "empty":
		logger, err = log.NewDefaultEmptyLogger()

	case "syslog":
		logger, err = log.NewDefaultSyslogLogger(c.Tag)

	default:
		logger, err = log.NewDefaultLogrusLogger()
	}
	if err != nil {
		return nil, err
	}

	if c.Level != nil {
		logger.SetLevel(*c.Level)
	} else {
		logger.SetLevel(log.DebugLevel)
	}

	return logger, nil
}
