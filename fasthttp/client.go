package fasthttp

import (
	"errors"
	"net"
	"time"

	"github.com/valyala/fasthttp"

	utilsTLS "gitlab.com/miatel/go/utils/tls"
)

type ClientConfig struct {
	DialTimeout         *uint                  `env:"DIAL_TIMEOUT" yaml:"dial_timeout"`
	TLSConfig           *utilsTLS.ClientConfig `env:"TLS_CONFIG" yaml:"tls_config"`
	MaxConnsPerHost     *uint                  `env:"MAX_CONNS_PER_HOST" yaml:"max_conns_per_host"`
	MaxIdleConnDuration *uint                  `env:"MAX_IDLE_CONN_DURATION" yaml:"max_idle_conn_duration"`
	MaxConnDuration     *uint                  `env:"MAX_CONN_DURATION" yaml:"max_conn_duration"`
	ReadTimeout         *uint                  `env:"READ_TIMEOUT" yaml:"read_timeout"`
	WriteTimeout        *uint                  `env:"WRITE_TIMEOUT" yaml:"write_timeout"`
	MaxConnWaitTimeout  *uint                  `env:"MAX_CONN_WAIT_TIMEOUT" yaml:"max_conn_wait_timeout"`
}

func (v *ClientConfig) Client() (*fasthttp.Client, error) {
	var err error

	client := &fasthttp.Client{}

	if v.DialTimeout != nil {
		timeout := time.Duration(*v.DialTimeout) * time.Millisecond
		if timeout != fasthttp.DefaultDialTimeout {
			client.Dial = func(addr string) (net.Conn, error) {
				return fasthttp.DialTimeout(addr, timeout)
			}
		}
	}

	if v.TLSConfig != nil {
		if client.TLSConfig, err = v.TLSConfig.TLSConfig(); err != nil {
			return nil, err
		}
	}

	if v.MaxConnsPerHost != nil {
		client.MaxConnsPerHost = int(*v.MaxConnsPerHost)
	} else {
		client.MaxConnsPerHost = fasthttp.DefaultMaxConnsPerHost
	}

	if v.MaxIdleConnDuration != nil {
		client.MaxIdleConnDuration = time.Duration(*v.MaxIdleConnDuration) * time.Millisecond
	} else {
		client.MaxIdleConnDuration = fasthttp.DefaultMaxIdleConnDuration
	}

	if v.MaxConnDuration != nil {
		client.MaxConnDuration = time.Duration(*v.MaxConnDuration) * time.Millisecond
	}

	if v.ReadTimeout != nil {
		client.ReadTimeout = time.Duration(*v.ReadTimeout) * time.Millisecond
	} else {
		client.ReadTimeout = time.Duration(5) * time.Second
	}

	if v.WriteTimeout != nil {
		client.WriteTimeout = time.Duration(*v.WriteTimeout) * time.Millisecond
	} else {
		client.WriteTimeout = time.Duration(5) * time.Second
	}

	if v.MaxConnWaitTimeout != nil {
		client.MaxConnWaitTimeout = time.Duration(*v.MaxConnWaitTimeout) * time.Millisecond
	} else {
		client.MaxConnWaitTimeout = time.Duration(1) * time.Second
	}

	return client, nil
}

func (v *ClientConfig) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.DialTimeout != nil && *v.DialTimeout < 100 {
		return errors.New("'dial_timeout' too small")
	}

	return nil
}
