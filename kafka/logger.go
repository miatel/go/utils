package kafka

import (
	"github.com/twmb/franz-go/pkg/kgo"
	"gitlab.com/miatel/go/log"
)

type Logger struct {
	impl log.Logger
}

func NewLogger(logger log.Logger) *Logger {
	return &Logger{
		impl: logger,
	}
}

func (v *Logger) Log(level kgo.LogLevel, msg string, kv ...interface{}) {
	logger := v.impl

	if len(kv) > 1 {
		fields := make(log.Fields)
		for i := 0; i < len(kv); i += 2 {
			fields[kv[i].(string)] = kv[i+1]
		}
		logger = logger.WithFields(fields)
	}

	switch level {
	case kgo.LogLevelNone:
	case kgo.LogLevelError:
		logger.Error(msg)
	case kgo.LogLevelWarn:
		logger.Warn(msg)
	case kgo.LogLevelInfo:
		logger.Info(msg)
	default:
		logger.Debug(msg)
	}
}

func (v *Logger) Level() (level kgo.LogLevel) {
	switch v.impl.GetLevel() {
	case log.PanicLevel:
		fallthrough
	case log.FatalLevel:
		fallthrough
	case log.ErrorLevel:
		level = kgo.LogLevelError
	case log.WarnLevel:
		level = kgo.LogLevelWarn
	case log.NoticeLevel:
		level = kgo.LogLevelWarn
	case log.InfoLevel:
		level = kgo.LogLevelInfo
	default:
		level = kgo.LogLevelDebug
	}

	return level
}
