package kafka

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/twmb/franz-go/pkg/kgo"
	"gitlab.com/miatel/go/log"

	utilsTLS "gitlab.com/miatel/go/utils/tls"
)

type Config struct {
	LogLevel    *log.Level             `env:"LOG_LEVEL" yaml:"log_level"`
	SeedBrokers []string               `env:"SEED_BROKERS" yaml:"seed_brokers"`
	DialTimeout *uint                  `env:"DIAL_TIMEOUT" yaml:"dial_timeout"`
	TLSConfig   *utilsTLS.ClientConfig `env:"TLS_CONFIG" yaml:"tls_config"`
	Consumer    *ConsumerConfig        `env:"CONSUMER" yaml:"consumer"`
	Producer    *ProducerConfig        `env:"PRODUCER" yaml:"producer"`
}

type ConsumerConfig struct {
	ConsumeResetOffset string   `env:"CONSUME_RESET_OFFSET" yaml:"consume_reset_offset"`
	ConsumerGroup      string   `env:"CONSUMER_GROUP" yaml:"consumer_group"`
	ConsumeTopics      []string `env:"CONSUME_TOPICS" yaml:"consume_topics"`
	InstanceID         string   `env:"INSTANCE_ID" yaml:"instance_id"`
	Rack               string   `env:"RACK" yaml:"rack"`
	SessionTimeout     *uint    `env:"SESSION_TIMEOUT" yaml:"session_timeout"`
	RebalanceTimeout   *uint    `env:"REBALANCE_TIMEOUT" yaml:"rebalance_timeout"`
}

type ProducerConfig struct {
	DefaultTopic          string   `env:"DEFAULT_TOPIC" yaml:"default_topic"`
	ProducerLinger        *uint    `env:"PRODUCER_LINGER" yaml:"producer_linger"`
	RecordDeliveryTimeout *uint    `env:"RECORD_DELIVERY_TIMEOUT" yaml:"record_delivery_timeout"`
	RecordRetries         *uint    `env:"RECORD_RETRIES" yaml:"record_retries"`
	TransactionTimeout    *uint    `env:"TRANSACTION_TIMEOUT" yaml:"transaction_timeout"`
	TransactionalID       string   `env:"TRANSACTIONAL_ID" yaml:"transactional_id"`
	Compression           []string `env:"COMPRESSION" yaml:"compression"`
}

func (v *Config) Opts(logger log.Logger) ([]kgo.Opt, error) {
	logger = logger.WithPrefix("kafka-client")
	if v.LogLevel != nil {
		logger.SetLevel(*v.LogLevel)
	}

	opts := []kgo.Opt{
		kgo.WithLogger(NewLogger(logger)),
		kgo.SeedBrokers(v.SeedBrokers...),
	}

	if v.DialTimeout != nil {
		opts = append(opts, kgo.DialTimeout(time.Duration(*v.DialTimeout)*time.Millisecond))
	}

	if v.TLSConfig != nil {
		if tlsConfig, err := v.TLSConfig.TLSConfig(); err != nil {
			return nil, err
		} else {
			opts = append(opts, kgo.DialTLSConfig(tlsConfig))
		}
	}

	if v.Consumer != nil {
		opts = append(opts, kgo.ConsumerGroup(v.Consumer.ConsumerGroup))
		opts = append(opts, kgo.ConsumeTopics(v.Consumer.ConsumeTopics...))
		opts = append(opts, kgo.RequireStableFetchOffsets())
		opts = append(opts, kgo.FetchIsolationLevel(kgo.ReadCommitted()))
		opts = append(opts, kgo.Rack(v.Consumer.Rack))

		if v.Consumer.InstanceID != "" {
			opts = append(opts, kgo.InstanceID(v.Consumer.InstanceID))
		}

		if v.Consumer.SessionTimeout != nil {
			opts = append(opts, kgo.SessionTimeout(time.Duration(*v.Consumer.SessionTimeout)*time.Millisecond))
		}

		if v.Consumer.RebalanceTimeout != nil {
			opts = append(opts, kgo.RebalanceTimeout(time.Duration(*v.Consumer.RebalanceTimeout)*time.Millisecond))
		}

		switch strings.ToLower(v.Consumer.ConsumeResetOffset) {
		case "start":
			opts = append(opts, kgo.ConsumeResetOffset(kgo.NewOffset().AtStart()))
		case "end":
			opts = append(opts, kgo.ConsumeResetOffset(kgo.NewOffset().AtEnd()))
		default:
			return nil, errors.New("unknown 'consume_reset_offset'")
		}
	}

	if v.Producer != nil {
		opts = append(opts, kgo.DefaultProduceTopic(v.Producer.DefaultTopic))

		if v.Producer.TransactionTimeout != nil {
			opts = append(opts, kgo.TransactionTimeout(time.Duration(*v.Producer.TransactionTimeout)*time.Millisecond))
		}

		if v.Producer.ProducerLinger != nil {
			opts = append(opts, kgo.ProducerLinger(time.Duration(*v.Producer.ProducerLinger)*time.Millisecond))
		}

		if v.Producer.RecordDeliveryTimeout != nil {
			opts = append(opts, kgo.RecordDeliveryTimeout(time.Duration(*v.Producer.RecordDeliveryTimeout)*time.Millisecond))
		}

		if v.Producer.RecordRetries != nil {
			opts = append(opts, kgo.RecordRetries(int(*v.Producer.RecordRetries)))
		}

		if v.Producer.TransactionalID != "" {
			transactionalID := v.Producer.TransactionalID + "-" + strconv.FormatInt(int64(os.Getpid()), 10)
			opts = append(opts, kgo.TransactionalID(transactionalID))
		}

		var codecs []kgo.CompressionCodec
		if len(v.Producer.Compression) > 0 {
			for _, codec := range v.Producer.Compression {
				switch strings.ToLower(codec) {
				case "none":
					codecs = append(codecs, kgo.NoCompression())
				case "gzip":
					codecs = append(codecs, kgo.GzipCompression())
				case "snappy":
					codecs = append(codecs, kgo.SnappyCompression())
				case "lz4":
					codecs = append(codecs, kgo.Lz4Compression())
				case "zstd":
					codecs = append(codecs, kgo.ZstdCompression())
				default:
					return nil, fmt.Errorf("unknown compression codec: %q", codec)
				}
			}
		} else {
			codecs = []kgo.CompressionCodec{kgo.ZstdCompression(), kgo.NoCompression()}
		}
		opts = append(opts, kgo.ProducerBatchCompression(codecs...))
	}

	return opts, nil
}

func (v *Config) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.Consumer == nil && v.Producer == nil {
		return errors.New("'consumer' or 'producer' must be defined")
	}

	if v.Consumer != nil {
		if v.Consumer.ConsumerGroup == "" {
			return errors.New("'consumer.consumer_group' not defined")
		}
		if len(v.Consumer.ConsumeTopics) < 1 {
			return errors.New("'consumer.consume_topics' not defined")
		}
	}
	if v.Producer != nil {
		if v.Producer.TransactionalID == "" {
			return errors.New("'producer.transactional_id' not defined")
		}
	}

	return nil
}
