package http

import (
	"errors"
	"net"
	"net/http"
	"time"

	utilsTLS "gitlab.com/miatel/go/utils/tls"
)

type ClientConfig struct {
	Transport *TransportConfig `env:"TRANSPORT" yaml:"transport"`
	Timeout   uint             `env:"TIMEOUT" yaml:"timeout"`
}

type TransportConfig struct {
	DialTimeout           *uint                  `env:"DIAL_TIMEOUT" yaml:"dial_timeout"`
	TLSConfig             *utilsTLS.ClientConfig `env:"TLS_CONFIG" yaml:"tls_config"`
	TLSHandshakeTimeout   *uint                  `env:"TLS_HANDSHAKE_TIMEOUT" yaml:"tls_handshake_timeout"`
	MaxIdleConns          *uint                  `env:"MAX_IDLE_CONNS" yaml:"max_idle_conns"`
	MaxIdleConnsPerHost   *uint                  `env:"MAX_IDLE_CONNS_PER_HOST" yaml:"max_idle_conns_per_host"`
	IdleConnTimeout       *uint                  `env:"IDLE_CONN_TIMEOUT" yaml:"idle_conn_timeout"`
	ResponseHeaderTimeout *uint                  `env:"RESPONSE_HEADER_TIMEOUT" yaml:"response_header_timeout"`
	ForceAttemptHTTP2     *bool                  `env:"FORCE_ATTEMPT_HTTP_2" yaml:"force_attempt_http2"`
}

func (v *ClientConfig) Client() (*http.Client, error) {
	client := &http.Client{
		Timeout: time.Duration(v.Timeout) * time.Millisecond,
	}

	if v.Transport != nil {
		transport := &http.Transport{
			ExpectContinueTimeout: time.Duration(1) * time.Second,
		}

		if v.Transport.DialTimeout != nil {
			dialer := &net.Dialer{
				Timeout:   time.Duration(*v.Transport.DialTimeout) * time.Millisecond,
				KeepAlive: time.Duration(30) * time.Second,
			}
			transport.DialContext = dialer.DialContext
		} else {
			dialer := &net.Dialer{
				Timeout:   time.Duration(30) * time.Second,
				KeepAlive: time.Duration(30) * time.Second,
			}
			transport.DialContext = dialer.DialContext
		}

		var err error
		if v.Transport.TLSConfig != nil {
			if transport.TLSClientConfig, err = v.Transport.TLSConfig.TLSConfig(); err != nil {
				return nil, err
			}
		}

		if v.Transport.TLSHandshakeTimeout != nil {
			transport.TLSHandshakeTimeout = time.Duration(*v.Transport.TLSHandshakeTimeout) * time.Millisecond
		} else {
			transport.TLSHandshakeTimeout = time.Duration(10) * time.Second
		}

		if v.Transport.MaxIdleConns != nil {
			transport.MaxIdleConns = int(*v.Transport.MaxIdleConns)
		} else {
			transport.MaxIdleConns = 100
		}

		if v.Transport.MaxIdleConnsPerHost != nil {
			transport.MaxIdleConnsPerHost = int(*v.Transport.MaxIdleConnsPerHost)
		}

		if v.Transport.IdleConnTimeout != nil {
			transport.IdleConnTimeout = time.Duration(*v.Transport.IdleConnTimeout) * time.Millisecond
		} else {
			transport.IdleConnTimeout = time.Duration(90) * time.Second
		}

		if v.Transport.ResponseHeaderTimeout != nil {
			transport.ResponseHeaderTimeout = time.Duration(*v.Transport.ResponseHeaderTimeout) * time.Millisecond
		}

		if v.Transport.ForceAttemptHTTP2 != nil {
			transport.ForceAttemptHTTP2 = *v.Transport.ForceAttemptHTTP2
		} else {
			transport.ForceAttemptHTTP2 = true
		}

		client.Transport = transport
	}

	return client, nil
}

func (v *ClientConfig) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.Timeout != 0 && v.Timeout < 1000 {
		return errors.New("'timeout' too small")
	}

	return nil
}
