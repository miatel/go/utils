package http

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	utilsTLS "gitlab.com/miatel/go/utils/tls"
)

type ServerConfig struct {
	Address     string                 `env:"ADDRESS" yaml:"address"`
	Port        uint16                 `env:"PORT" yaml:"port"`
	TLSConfig   *utilsTLS.ServerConfig `env:"TLS_CONFIG" yaml:"tls_config"`
	IdleTimeout uint                   `env:"IDLE_TIMEOUT" yaml:"idle_timeout"`
}

func (v *ServerConfig) Server() (*http.Server, error) {
	server := &http.Server{
		Addr:        fmt.Sprintf("%s:%d", v.Address, v.Port),
		IdleTimeout: time.Duration(v.IdleTimeout) * time.Millisecond,
	}

	if v.TLSConfig != nil {
		var err error
		if server.TLSConfig, err = v.TLSConfig.TLSConfig(); err != nil {
			return nil, err
		}
	}

	return server, nil
}

func (v *ServerConfig) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.Address == "" {
		return errors.New("'address' not defined")
	}
	if v.Port < 1 {
		return errors.New("'port' invalid value")
	}
	if v.IdleTimeout != 0 && v.IdleTimeout < 1000 {
		return errors.New("'idle_timeout' too small")
	}

	return nil
}
