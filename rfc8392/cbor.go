package rfc8392

import (
	"github.com/fxamacker/cbor/v2"
)

// Pre-configured modes for CBOR encoding and decoding.
var (
	encMode cbor.EncMode
	decMode cbor.DecMode
)

func init() {
	var err error

	// init encode mode
	encOpts := cbor.EncOptions{
		Sort:        cbor.SortCanonical,        // sort map keys
		IndefLength: cbor.IndefLengthForbidden, // no streaming
	}
	encMode, err = encOpts.EncMode()
	if err != nil {
		panic(err)
	}

	// init decode mode
	decOpts := cbor.DecOptions{
		DupMapKey:        cbor.DupMapKeyEnforcedAPF, // duplicated key not allowed
		MaxNestedLevels:  8,                         // max nested levels allowed for any combination of CBOR array, maps, and tags
		MaxArrayElements: 128,                       // max number of elements for CBOR arrays
		MaxMapPairs:      128,                       // max number of key-value pairs for CBOR maps
		IndefLength:      cbor.IndefLengthForbidden, // no streaming
		IntDec:           cbor.IntDecConvertSigned,  // decode CBOR uint/int to Go int64
	}
	decMode, err = decOpts.DecMode()
	if err != nil {
		panic(err)
	}
}
