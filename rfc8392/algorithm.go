package rfc8392

import (
	"fmt"
)

// Algorithms supported by this library
const (
	// AlgorithmEd25519 PureEdDSA by RFC 8152.
	AlgorithmEd25519 Algorithm = iota
)

type Algorithm int

// String returns the name of the algorithm
func (v Algorithm) String() string {
	switch v {
	case AlgorithmEd25519:
		return "EdDSA"

	default:
		return fmt.Sprintf("unknown algorithm value %d", v)
	}
}
