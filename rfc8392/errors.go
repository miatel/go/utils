package rfc8392

import "errors"

var (
	ErrAlgorithmNotSupported = errors.New("algorithm not supported")
)
