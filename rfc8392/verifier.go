package rfc8392

import (
	"crypto"

	"github.com/veraison/go-cose"
)

type Verifier struct {
	verifier cose.Verifier
}

func NewVerifier(alg Algorithm, key crypto.PublicKey) (*Verifier, error) {
	var (
		err      error
		verifier cose.Verifier
	)

	switch alg {
	case AlgorithmEd25519:
		verifier, err = cose.NewVerifier(cose.AlgorithmEd25519, key)

	default:
		err = ErrAlgorithmNotSupported
	}

	if err != nil {
		return nil, err
	}

	return &Verifier{verifier: verifier}, nil
}

func (v *Verifier) Verify(raw []byte, data any) error {
	var err error
	var msg cose.Sign1Message

	err = msg.UnmarshalCBOR(raw)
	if err != nil {
		return err
	}

	err = msg.Verify(nil, v.verifier)
	if err != nil {
		return err
	}

	if err = decMode.Unmarshal(msg.Payload, data); err != nil {
		return err
	}

	return nil
}
