package rfc8392

import (
	"crypto"
	"crypto/rand"

	"github.com/veraison/go-cose"
)

type Signer struct {
	signer cose.Signer
}

func NewSigner(alg Algorithm, key crypto.Signer) (*Signer, error) {
	var (
		err    error
		signer cose.Signer
	)

	switch alg {
	case AlgorithmEd25519:
		signer, err = cose.NewSigner(cose.AlgorithmEd25519, key)

	default:
		err = ErrAlgorithmNotSupported
	}

	if err != nil {
		return nil, err
	}

	return &Signer{signer: signer}, nil
}

func (s *Signer) Sign(data any) ([]byte, error) {
	payload, err := encMode.Marshal(data)
	if err != nil {
		return nil, err
	}

	headers := cose.Headers{
		Protected: cose.ProtectedHeader{
			cose.HeaderLabelAlgorithm: s.signer.Algorithm(),
		},
	}

	return cose.Sign1(rand.Reader, s.signer, headers, payload, nil)
}
