package rfc8392

type Iss string
type Sub string
type Aud string
type Exp int64
type Nbf int64
type Iat int64
type Cti []byte
