package log

import "time"

type Logger interface {
	Close() error

	Print(args ...any)

	Trace(args ...any)

	Debug(args ...any)

	Info(args ...any)

	Notice(args ...any)

	Warn(args ...any)

	Error(args ...any)

	Fatal(args ...any)

	Panic(args ...any)

	Printf(format string, args ...any)

	Tracef(format string, args ...any)

	Debugf(format string, args ...any)

	Infof(format string, args ...any)

	Noticef(format string, args ...any)

	Warnf(format string, args ...any)

	Errorf(format string, args ...any)

	Fatalf(format string, args ...any)

	Panicf(format string, args ...any)

	WithPrefix(prefix string) Logger

	Prefix() string

	WithFields(fields Fields) Logger

	Fields() Fields

	Elapsed(start time.Time) Logger

	GetLevel() Level

	SetLevel(level Level) Logger
}

type Loggable interface {
	Log() Logger
}
