package log

import (
	"fmt"
	"strings"
)

type Level uint32

const (
	// PanicLevel level, the highest level of severity.
	// Logs and then calls panic with the message passed to Debug, Info, ...
	PanicLevel Level = iota
	// FatalLevel level. Logs and then calls `logger.Exit(1)`.
	// It will exit even if the logging level is set to Panic.
	FatalLevel
	// ErrorLevel level. Logs. Used for errors that should definitely be noted.
	// Commonly used for hooks to send errors to an error tracking service.
	ErrorLevel
	// WarnLevel level. Non-critical entries that deserve eyes.
	WarnLevel
	// NoticeLevel level. Conditions that are not error conditions, but should possibly be handled specially.
	NoticeLevel
	// InfoLevel level. General operational entries about what's going on inside the application.
	InfoLevel
	// DebugLevel level. Usually only enabled when debugging. Very verbose logging.
	DebugLevel
	// TraceLevel level. Designates finer-grained informational events than the Debug.
	TraceLevel
)

func (level Level) String() string {
	if b, err := level.MarshalText(); err != nil {
		return "unknown"
	} else {
		return string(b)
	}
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (level *Level) UnmarshalText(text []byte) error {
	var l Level

	switch strings.ToLower(string(text)) {
	case "panic":
		l = PanicLevel
	case "fatal":
		l = FatalLevel
	case "error":
		l = ErrorLevel
	case "warn", "warning":
		l = WarnLevel
	case "notice":
		l = NoticeLevel
	case "info":
		l = InfoLevel
	case "debug":
		l = DebugLevel
	case "trace":
		l = TraceLevel
	default:
		return fmt.Errorf("unknown log level: %q", string(text))
	}

	*level = l

	return nil
}

func (level Level) MarshalText() ([]byte, error) {
	switch level {
	case PanicLevel:
		return []byte("panic"), nil
	case FatalLevel:
		return []byte("fatal"), nil
	case ErrorLevel:
		return []byte("error"), nil
	case WarnLevel:
		return []byte("warning"), nil
	case NoticeLevel:
		return []byte("notice"), nil
	case InfoLevel:
		return []byte("info"), nil
	case DebugLevel:
		return []byte("debug"), nil
	case TraceLevel:
		return []byte("trace"), nil
	}

	return nil, fmt.Errorf("unknown log level: %d", level)
}
