package utils_ed25519

import (
	"crypto/ed25519"
	"encoding/base64"
	"errors"
)

type PublicKey ed25519.PublicKey

func (v *PublicKey) UnmarshalText(text []byte) error {
	dst := make([]byte, base64.StdEncoding.DecodedLen(len(text)))
	n, err := base64.StdEncoding.Decode(dst, text)
	if err != nil {
		return err
	}

	if n != ed25519.PublicKeySize {
		return errors.New("wrong PublicKey size")
	}

	dst = dst[:n]

	*v = dst

	return nil
}
