package utils_ed25519_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"

	utilsED25519 "gitlab.com/miatel/go/utils/ed25519"
)

func TestPublicKey_UnmarshalText(t *testing.T) {
	testCases := []struct {
		Name        string
		Value       []byte
		ResultKey   utilsED25519.PublicKey
		ResultError error
	}{
		{
			Name:  "Valid",
			Value: []byte("YNLgfTkKgu1/RedbdzwM9nCpwyheOpHrSuql45J9cKQ="),
			ResultKey: utilsED25519.PublicKey{
				0x60, 0xd2, 0xe0, 0x7d, 0x39, 0xa, 0x82, 0xed, 0x7f, 0x45, 0xe7, 0x5b, 0x77, 0x3c, 0xc, 0xf6,
				0x70, 0xa9, 0xc3, 0x28, 0x5e, 0x3a, 0x91, 0xeb, 0x4a, 0xea, 0xa5, 0xe3, 0x92, 0x7d, 0x70, 0xa4,
			},
			ResultError: nil,
		},
		{
			Name:        "Wrong size with empty value",
			Value:       []byte(""),
			ResultKey:   nil,
			ResultError: errors.New("wrong PublicKey size"),
		},
		{
			Name:        "Wrong size with nil value",
			Value:       nil,
			ResultKey:   nil,
			ResultError: errors.New("wrong PublicKey size"),
		},
		{
			Name:        "illegal base64 data #1",
			Value:       []byte("бэйс64"),
			ResultKey:   nil,
			ResultError: errors.New("illegal base64 data at input byte 0"),
		},
		{
			Name:        "illegal base64 data #2",
			Value:       []byte("Q-FsbGJhY2s="),
			ResultKey:   nil,
			ResultError: errors.New("illegal base64 data at input byte 1"),
		},
		{
			Name:        "illegal base64 data #3",
			Value:       []byte("Q2FsbGJhY2s"),
			ResultKey:   nil,
			ResultError: errors.New("illegal base64 data at input byte 8"),
		},
	}

	for _, test := range testCases {
		t.Run(test.Name, func(t *testing.T) {
			require := require.New(t)

			var key utilsED25519.PublicKey
			err := key.UnmarshalText(test.Value)
			if test.ResultError != nil {
				require.Equal(test.ResultKey, key)
				require.Error(err)
				require.Equal(test.ResultError.Error(), err.Error())
			} else {
				require.Equal(test.ResultKey, key)
				require.NoError(err)
			}
		})
	}
}
