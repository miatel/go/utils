package utils_ed25519

import (
	"crypto/ed25519"
	"encoding/base64"
	"errors"
)

type PrivateKey ed25519.PrivateKey

func (v *PrivateKey) UnmarshalText(text []byte) error {
	dst := make([]byte, base64.StdEncoding.DecodedLen(len(text)))
	n, err := base64.StdEncoding.Decode(dst, text)
	if err != nil {
		return err
	}

	if n != ed25519.SeedSize {
		return errors.New("wrong PrivateKey Seed size")
	}

	dst = dst[:n]

	*v = PrivateKey(ed25519.NewKeyFromSeed(dst))

	return nil
}
