package utils_ed25519_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"

	utilsED25519 "gitlab.com/miatel/go/utils/ed25519"
)

func TestPrivateKey_UnmarshalText(t *testing.T) {
	testCases := []struct {
		Name        string
		Value       []byte
		ResultKey   utilsED25519.PrivateKey
		ResultError error
	}{
		{
			Name:  "Valid",
			Value: []byte("BR+wGrNwzewzta6xbQTPhPTxas1/wyxM0J+NJIu+ttM="),
			ResultKey: utilsED25519.PrivateKey{
				0x5, 0x1f, 0xb0, 0x1a, 0xb3, 0x70, 0xcd, 0xec, 0x33, 0xb5, 0xae, 0xb1, 0x6d, 0x4, 0xcf, 0x84,
				0xf4, 0xf1, 0x6a, 0xcd, 0x7f, 0xc3, 0x2c, 0x4c, 0xd0, 0x9f, 0x8d, 0x24, 0x8b, 0xbe, 0xb6, 0xd3,
				0x60, 0xd2, 0xe0, 0x7d, 0x39, 0xa, 0x82, 0xed, 0x7f, 0x45, 0xe7, 0x5b, 0x77, 0x3c, 0xc, 0xf6,
				0x70, 0xa9, 0xc3, 0x28, 0x5e, 0x3a, 0x91, 0xeb, 0x4a, 0xea, 0xa5, 0xe3, 0x92, 0x7d, 0x70, 0xa4,
			},
			ResultError: nil,
		},
		{
			Name:        "Wrong Seed size with empty value",
			Value:       []byte(""),
			ResultKey:   nil,
			ResultError: errors.New("wrong PrivateKey Seed size"),
		},
		{
			Name:        "Wrong Seed size with nil value",
			Value:       nil,
			ResultKey:   nil,
			ResultError: errors.New("wrong PrivateKey Seed size"),
		},
		{
			Name:        "illegal base64 data #1",
			Value:       []byte("бэйс64"),
			ResultKey:   nil,
			ResultError: errors.New("illegal base64 data at input byte 0"),
		},
		{
			Name:        "illegal base64 data #2",
			Value:       []byte("Q-FsbGJhY2s="),
			ResultKey:   nil,
			ResultError: errors.New("illegal base64 data at input byte 1"),
		},
		{
			Name:        "illegal base64 data #3",
			Value:       []byte("Q2FsbGJhY2s"),
			ResultKey:   nil,
			ResultError: errors.New("illegal base64 data at input byte 8"),
		},
	}

	for _, test := range testCases {
		t.Run(test.Name, func(t *testing.T) {
			require := require.New(t)

			var key utilsED25519.PrivateKey
			err := key.UnmarshalText(test.Value)
			if test.ResultError != nil {
				require.Equal(test.ResultKey, key)
				require.Error(err)
				require.Equal(test.ResultError.Error(), err.Error())
			} else {
				require.Equal(test.ResultKey, key)
				require.NoError(err)
			}
		})
	}
}
