package config

import (
	"bufio"
	"fmt"
	"io"
	"os"

	"gitlab.com/miatel/go/log"
	"gopkg.in/yaml.v3"

	utilsLogger "gitlab.com/miatel/go/utils/logger"
)

type Complete interface {
	Default()
	LoggerConfig() *utilsLogger.Config
	Validate() error
}

const defaultConfigFile = "config.yml"

func load(config Complete) error {
	var (
		err  error
		name string
		file *os.File
	)

	if len(os.Args) < 2 || os.Args[1] == "" {
		name = defaultConfigFile
	} else {
		name = os.Args[1]
	}

	if file, err = os.Open(name); err != nil {
		return err
	}

	config.Default()

	dec := yaml.NewDecoder(bufio.NewReader(file))
	dec.KnownFields(true)
	if err = dec.Decode(config); err != nil && err != io.EOF {
		return err
	}

	return nil
}

func Load(config Complete) (log.Logger, error) {
	var (
		err    error
		logger log.Logger
	)

	if err = load(config); err != nil {
		return nil, fmt.Errorf("load config error: %v", err)
	}

	if err = config.Validate(); err != nil {
		return nil, fmt.Errorf("validate config error: %v", err)
	}

	if logger, err = utilsLogger.New(config.LoggerConfig()); err != nil {
		return nil, fmt.Errorf("create logger error: %v", err)
	}

	return logger, nil
}
