package config_test

import (
	"os"
	"testing"

	utilsConfig "gitlab.com/miatel/go/utils/config"
	utilsGeneral "gitlab.com/miatel/go/utils/general"
	utilsLogger "gitlab.com/miatel/go/utils/logger"
)

type Config struct {
	General *utilsGeneral.Config `yaml:"general"`
}

func (v *Config) Default() {
	*v = Config{
		General: &utilsGeneral.Config{},
	}
	v.General.Default()
}

func (v *Config) LoggerConfig() *utilsLogger.Config {
	return v.General.Log
}

func (v *Config) Validate() error {
	return nil
}

func TestConfig_Load(t *testing.T) {
	os.Args = []string{"config_test", "complete_test.yml"}

	c := &Config{}
	if logger, err := utilsConfig.Load(c); err != nil {
		t.Fatal(err)
	} else {
		logger.Close()
	}
}
