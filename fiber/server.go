package utils_fiber

import (
	"crypto/tls"
	"errors"
	"time"

	"github.com/gofiber/fiber/v2"

	utilsTLS "gitlab.com/miatel/go/utils/tls"
)

type ServerConfig struct {
	Network string `env:"NETWORK" yaml:"network"`
	Address string `env:"ADDRESS" yaml:"address"`
	Port    uint16 `env:"PORT" yaml:"port"`

	UtilsTLSConfig *utilsTLS.ServerConfig `env:"TLS_CONFIG" yaml:"tls_config"`

	ProxyForwardedAddrHeader string `env:"PROXY_FORWARDED_ADDR_HEADER" yaml:"proxy_forwarded_addr_header"`

	ReadTimeout  uint `env:"READ_TIMEOUT" yaml:"read_timeout"`
	WriteTimeout uint `env:"WRITE_TIMEOUT" yaml:"write_timeout"`
	IdleTimeout  uint `env:"IDLE_TIMEOUT" yaml:"idle_timeout"`

	DisableDefaultDate        bool `env:"DISABLE_DEFAULT_DATE" yaml:"disable_default_date"`
	DisableDefaultContentType bool `env:"DISABLE_DEFAULT_CONTENT_TYPE" yaml:"disable_default_content_type"`
}

func (v *ServerConfig) FiberServerConfig() fiber.Config {
	config := fiber.Config{
		Network:                   v.Network,
		ReadTimeout:               time.Duration(v.ReadTimeout) * time.Millisecond,
		WriteTimeout:              time.Duration(v.WriteTimeout) * time.Millisecond,
		IdleTimeout:               time.Duration(v.IdleTimeout) * time.Millisecond,
		ProxyHeader:               v.ProxyForwardedAddrHeader,
		DisableDefaultDate:        v.DisableDefaultDate,
		DisableDefaultContentType: v.DisableDefaultContentType,
		DisableStartupMessage:     true,
	}

	return config
}

func (v *ServerConfig) TLSConfig() (config *tls.Config, err error) {
	if v.UtilsTLSConfig != nil {
		return v.UtilsTLSConfig.TLSConfig()
	}

	return nil, nil
}

func (v *ServerConfig) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.Address == "" {
		return errors.New("'address' not defined")
	}
	if v.Port < 1 {
		return errors.New("'port' invalid value")
	}
	if v.IdleTimeout != 0 && v.IdleTimeout < 1000 {
		return errors.New("'idle_timeout' too small")
	}

	return nil
}
