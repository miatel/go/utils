package cwt

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
)

func New[T any, PT Claims[T]](options ...Option) fiber.Handler {
	var err error

	cfg := &config{}
	for _, opt := range options {
		opt(cfg)
	}
	cfg.defaults()

	if cfg.verifier == nil {
		panic("CWT verifier not defined")
	}

	tokenFunc := cfg.tokenFunc()
	verifier := cfg.verifier
	contextKey := cfg.contextKey

	return func(c *fiber.Ctx) error {
		var token []byte
		if token, err = tokenFunc(c); err != nil {
			return &Error{fmt.Sprintf("get token error: %v", err)}
		}

		claims := PT(new(T))
		claims.Default()
		if err = verifier.Verify(token, claims); err != nil {
			return &Error{fmt.Sprintf("verify token error: %v", err)}
		}
		if err = claims.Validate(); err != nil {
			return &Error{fmt.Sprintf("validate token error: %v", err)}
		}

		c.Locals(contextKey, claims)

		return c.Next()
	}
}
