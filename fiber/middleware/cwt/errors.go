package cwt

type Error struct {
	Msg string
}

func (e *Error) Error() string {
	return e.Msg
}

var (
	ErrMissingToken = &Error{"missing or malformed token"}
)
