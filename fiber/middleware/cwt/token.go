package cwt

import (
	"github.com/gofiber/fiber/v2"
)

const (
	Header TokenSourceType = iota
	Query
	Param
	Cookie
)

type TokenFunc func(c *fiber.Ctx) ([]byte, error)
type TokenSourceType int
