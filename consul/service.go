package consul

import (
	"context"
	"fmt"

	consulAPI "github.com/hashicorp/consul/api"
	"gitlab.com/miatel/go/log"
)

type Service struct {
	logger log.Logger

	client *consulAPI.Client

	id   string
	name string
}

func New(config *ServiceConfig, logger log.Logger) (*Service, error) {
	var err error

	logger = logger.WithPrefix("consul")
	if config.LogLevel != nil {
		logger.SetLevel(*config.LogLevel)
	}

	s := &Service{
		logger: logger,
		id:     config.ID,
		name:   config.Name,
	}

	clientConfig := consulAPI.DefaultConfig()
	clientConfig.Token = config.Token
	if config.HTTPConfig != nil {
		if clientConfig.HttpClient, err = config.HTTPConfig.Client(); err != nil {
			return nil, err
		}
	}

	if s.client, err = consulAPI.NewClient(clientConfig); err != nil {
		return nil, err
	}

	return s, nil
}

func (s *Service) Log() log.Logger {
	return s.logger
}

func (s *Service) Register(ctx context.Context, address string, port int) error {
	reg := &consulAPI.AgentServiceRegistration{
		ID:      s.id,
		Name:    s.name,
		Port:    port,
		Address: address,
	}

	opts := consulAPI.ServiceRegisterOpts{
		ReplaceExistingChecks: false,
	}

	if ctx != nil {
		opts = opts.WithContext(ctx)
	}

	if err := s.client.Agent().ServiceRegisterOpts(reg, opts); err != nil {
		return fmt.Errorf("register \"%s:%d\" error: %w", address, port, err)
	}

	return nil
}

func (s *Service) Deregister() error {
	if err := s.client.Agent().ServiceDeregister(s.id); err != nil {
		return fmt.Errorf("deregister error: %w", err)
	}

	return nil
}
