#!/bin/sh

find . -mindepth 1 -maxdepth 1 -type d | sort | egrep -v '(.git|.idea|vendor)' | \
while read DIR; do
	echo "${DIR}"
	cd "${DIR}" || exit 1
	go get -u
	go mod tidy
	go mod vendor
	cd -
done

git add -A vendor
